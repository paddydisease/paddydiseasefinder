import base64
import os
from io import BytesIO

import pickledb
import requests
import tensorflow as tf
import numpy as np
from PIL import Image
from flask import *
import multiprocessing
from zipfile import ZipFile
import io
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas

from Predictor import Predictor
import logging

app = Flask(__name__)
db = pickledb.load('example.db', False)
db.set("Count", 0)
DiseasePredictor = None

def getModels(url, modelName="model.h5"):
    print("inside get models")
    modelRequest = requests.get(url)
    if modelRequest.status_code == requests.codes.ok:
        print(modelRequest)
        model = modelRequest.content
        file = open("MLModels/" + modelName, "wb")
        file.write(modelRequest.content)
        file.close()
        return model
    else:
        print(modelRequest.status_code)
        print('modelRetrivalFailed')


def startup():
    global DiseasePredictor
    status = "FAILURE"
    modelN = "DiseasePredictor.h5"
    print(f"Model Name : {modelN.lower()}")
    print("Model Downloading ...")
    URL = "https://gitlab.com/paddydisease/paddydiseasefinder/-/raw/8914b44cf3c60693d093ee48e6c7214cc4fa1106/Model/DiseasePredictor.h5"
    DiseasePredictor = getModels(URL, modelName="DiseasePredictor.h5")
    status = "SUCCESS"
    print(os.listdir())
startup()
logging.basicConfig(filename="LogFile/Logger.log",format='%(asctime)s %(message)s', filemode='a',datefmt='%m/%d/%Y %I:%M:%S %p')
Object = Predictor(tf.keras.models.load_model('MLModels/DiseasePredictor.h5'))

@app.route('/')
def home():
    global Object
    # logging.basicConfig(filename="LogFile/Logger.log",
    #                     format='%(asctime)s %(message)s', filemode='a',
    #                     datefmt='%m/%d/%Y %I:%M:%S %p')
    # Object = Predictor(tf.keras.models.load_model('MLModels/DiseasePredictor.h5'))
    return 'Hello, ServerMaintainer'


@app.route('/predict', methods=['GET', 'POST'])
def predict():
    if request.method == 'POST':

        global Object

        print("Prediction Started")

        # Getting the Params
        input_json = request.get_json(force=True)

        predictJobs = input_json['predictJobs']
        result_response = {"resultArray": [], "resultCount": 0}

        count = int(db.get("Count"))
        if not count:
            count = 0

        batchcount = 0
        count += 1

        # Prediction
        for i in predictJobs:
            count += 1
            uri = i
            db.set("" + str(count), uri)
            db.set("Count", count)
            binary_data = base64.b64decode(uri)
            # print(binary_data)
            img = Image.open(BytesIO(binary_data))
            # Save the image to a file
            rgb_im = img.convert('RGB')
            rgb_im.save(f"images/image{count}.jpg")

            image = tf.keras.preprocessing.image.load_img(f'images/image{count}.jpg', target_size=(128, 128))
            img = tf.keras.preprocessing.image.img_to_array(image)
            img = np.expand_dims(img, axis=0)
            PreprocessedImage = np.vstack([img])

            result_response['resultArray'].append(Object.Predict(PreprocessedImage))
            db.set("" + str(count), {'img': f"image{count}.jpg", 'batchCount': batchcount})
            batchcount += 1

        result_response['resultCount'] = len(result_response['resultArray'])

        return jsonify(result_response)

    else:
        return 'Unable to Precess Your Request'


@app.route('/paddy_zip_predict', methods=['POST'])
def paddy_zip_predict():
    file = request.files['file']
    if not file:
        return {"error": "No file uploaded."}, 400

    zip_data = io.BytesIO(file.read())
    results = []

    with ZipFile(zip_data, 'r') as zip_file:
        for file_name in zip_file.namelist():
            with zip_file.open(file_name) as img_file:
                img = Image.open(img_file)
                img.save(f"images/{file_name}.jpg")
                image = tf.keras.preprocessing.image.load_img(f'images/{file_name}.jpg', target_size=(128, 128))
                img = tf.keras.preprocessing.image.img_to_array(image)
                img = np.expand_dims(img, axis=0)
                PreprocessedImage = np.vstack([img])

                Result = Object.Predict(PreprocessedImage)
                results.append(Result)
    print(results)
    pdf_file = BytesIO()
    report = canvas.Canvas(pdf_file, pagesize=letter)
    y = 720
    for i in range(1, len(results) + 1):
        if i % 6 == 0:
            y = 720
            report.showPage()
        report.drawString(72, y, "Patient ID : {}".format(results[i - 1][0]))
        y -= 30
    report.save()

    pdf_file.seek(0)
    return send_file(pdf_file, as_attachment=True, download_name='report.pdf')


@app.route('/loadModel/<modelName>', methods=['GET'])
def getModel(modelName):
    global DiseasePredictor
    status = "FAILURE"
    print(f"Model Name : {modelName.lower()}")
    if modelName.lower() == 'diseasepredictor' or modelName == "1" or modelName == "all":
        print("Model Downloading ...")
        URL = "https://gitlab.com/paddydisease/paddydiseasefinder/-/raw/8914b44cf3c60693d093ee48e6c7214cc4fa1106/Model/DiseasePredictor.h5"
        DiseasePredictor = getModels(URL, modelName="DiseasePredictor.h5")
        status = "SUCCESS"
    print(os.listdir())
    return jsonify({"ModelLoading": status})


@app.route('/image/<Id>')
def getimage(Id):
    image_uri = db.get("" + str(Id))
    print(image_uri)
    if image_uri:
        return '<img src="data:image/jpeg;base64,' + image_uri + '">'
    else:
        return jsonify({'error': "No image found"})


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


# def startServer():
#     port = 8080
#     app.run(host="0.0.0.0", debug=True, port=int(port), use_reloader=False)
#
#
# def activateLogger():
#     # startup task
#     requests.get("http://127.0.0.1:8080")
#
#
# if __name__ == '__main__':
#     p1 = multiprocessing.Process(target=startServer)
#     p2 = multiprocessing.Process(target=activateLogger)
#
#     p1.start()
#     p2.start()
