import numpy as np


class Predictor:
    def __init__(self, Disease_Predictor):
        self.Disease_Predictor = Disease_Predictor


    @staticmethod
    def load_classes(predicted_class_index):
        Classes = {0: 'bacterial_leaf_blight', 1: 'bacterial_leaf_streak', 2: 'bacterial_panicle_blight', 3: 'blast',
                   4: 'brown_spot', 5: 'dead_heart', 6: 'downy_mildew', 7: 'hispa', 8: 'normal', 9: 'others',
                   10: 'tungro'}

        return Classes[predicted_class_index]

    def Predict(self, preprocessedImage):
        print("Prediction Started ....")

        prediction = self.Disease_Predictor.predict(preprocessedImage)

        predicted_class_index = np.argmax(prediction)
        print(predicted_class_index)
        predicted_class = self.load_classes(predicted_class_index)
        print("Prediction Completed ....")
        print(f"Predicted Class : {predicted_class}")
        return {"classPredicted": predicted_class}
