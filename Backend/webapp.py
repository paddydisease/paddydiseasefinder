import streamlit as st
import requests
from Predictor import Predictor
import tensorflow as tf
import numpy as np
from PIL import Image

st.set_page_config("🌾 Paddy Disease Finder", layout="wide")

tab = st.tabs(["🖼️ ImageUploader", "📁 ZipUploader", "📈 Result", "🤙 UpdateModel", "⚡ Logs", "ℹ️Info"])

with tab[0]:
    uploadedFiles = st.file_uploader("Upload Paddy Images", accept_multiple_files=True, type=['jpg', 'png', 'jpeg'])
    if uploadedFiles:
        for file in uploadedFiles:
            st.image(file, caption=file.name, width=400)
        if st.button("Analyze"):
            for file in uploadedFiles:
                Object = Predictor(tf.keras.models.load_model('MLModels/DiseasePredictor.h5'))
                img = Image.open(file)
                img.save("images/" + file.name)
                image = tf.keras.preprocessing.image.load_img("images/" + file.name, target_size=(128, 128))
                img = tf.keras.preprocessing.image.img_to_array(image)
                img = np.expand_dims(img, axis=0)
                PreprocessedImage = np.vstack([img])
                result = Object.Predict(PreprocessedImage)

                with tab[2]:
                    st.image(file, caption=file.name)
                    st.write(result)

with tab[1]:
    ZipFile = st.file_uploader("Upload The Zip File Containing Paddy Images", type=['zip'])

with tab[3]:
    st.write("Click this to update the latest model from source")
    if st.button("Update"):
        url = "http://127.0.0.1:8021/loadModel/DiseasePredictor"
        response = requests.get(url)
        print(response.status_code)
        status = response.json()
        st.write("Model Updation Request Completed --> For More Details kindly Check the logs")

with tab[4]:
    st.title("Logs")
    with open("LogFile/Logger.log", "r") as f:
        log_contents = f.read()
    st.write(log_contents)

with tab[5]:
    st.title("This is Page to Maintain the Paddy disease Prediction Application Server")
    st.write("If you have any Queries Please Feel Free to Contact Admin")


