#!/bin/bash

echo "Make sure all the files are in correct directory"
echo ""
echo "Server"
echo "----> StartServer.sh"
echo "----> StartWebsite.sh"
echo ""
echo "Backend"
echo "----> app.py"
echo "----> webapp.py"
echo  ""

cd ..
cd Backend || exit
streamlit run webapp.py [dark]