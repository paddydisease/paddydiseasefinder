#!/bin/bash

echo "Make Sure You Download all the Dependencies"
echo ""

echo "Make sure all the files are in correct directory"
echo ""
echo "Server"
echo "----> StartServer.sh"
echo "----> StartWebsite.sh"
echo ""
echo "Backend"
echo "----> app.py"
echo "----> webapp.py"
echo ""

echo "Enter 'Y' if you want to download the dependencies else 'n'"
# shellcheck disable=SC2162
read requirements

# shellcheck disable=SC2034
yes="Y"
# shellcheck disable=SC2034
no="n"

if [ "$requirements" == 'Y' ] || [ "$requirements" == 'y' ]
then
  cd ..
  cd Backend || exit
  pyhton3 -m pip install -r requirements.txt || exit
else
  echo
fi

cd ..
cd Backend || exit
python3 app.py
